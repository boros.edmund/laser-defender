﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] List<WaveConfig> waveConfigs;
    [SerializeField] bool looping = false;
    [SerializeField] int startingWave = 0;
    IEnumerator Start()
    {
        var currentWave = waveConfigs[startingWave];

        do
        {
            yield return StartCoroutine(SpawnAllWaves());
        } while (looping);
    }

    private IEnumerator SpawnAllWaves()
    {
        for(int i= startingWave; i < waveConfigs.Count; i++)
        {
            var currentWave = waveConfigs[i];
            yield return StartCoroutine(SpawnAllEnemiesIWave(currentWave));
        }
    }

    private IEnumerator SpawnAllEnemiesIWave(WaveConfig waveConfig)
    {
        for(int i=0; i<= waveConfig.NumberOfEnemies; i++)
        {
            var newEnemy = Instantiate(waveConfig.EnemyPrefab,
                                       waveConfig.GetWaypoints()[0].transform.position,
                                       Quaternion.identity);

            newEnemy.GetComponent<EnemyPathing>().SetWaveConfig(waveConfig);
            yield return new WaitForSeconds(waveConfig.TimeBetweenSpawns);
        }
      
    }
}
